//////////////////////////////////////////////////////////////////////
// Copyright(c) 2022-2022 *** All Rights Reserved
// Name:        isingmoudlesimulation.h
// Purpose:
// Author:      Shi Yuke
// Modified by:
// Created:     2022-09-25 16:04
// Version:     1.0.0
// Licence:
//////////////////////////////////////////////////////////////////////

#ifndef ISINGMOUDLESIMULATION_H
#define ISINGMOUDLESIMULATION_H

#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>

#include <math.h>

#include <random>
#include <time.h>

using std::map;
using std::vector;
using std::cout;
using std::endl;
using std::string;
using std::fstream;

class IsingMoudleSimulation {
public:
    /**
     * @brief IsingMoudleSimulation 构造函数
     */
    IsingMoudleSimulation();
    IsingMoudleSimulation(int size);
    //@bug将行列搞反，待改
//    IsingMoudleSimulation(int row, int col);

    /**
     * @brief setParam 设置参数
     */
    void setSize(int size);
//    void setSize(int row, int col);
    void setTemperature(double temperature);
    void setIterationTimes(int iterationTimes);
    void setIterationWays(bool random);

    /**
     * @brief iterateToEquilibrium 迭代n次达到平衡状态
     * @param random 1：随机选择翻转格点，0：遍历格点
     */
    void iterateToEquilibrium();
    void iterateToEquilibrium(long time);

    /**
     * @brief iterateForQualities 迭代n次计算物理量
     * @param random 1：随机选择翻转格点，0：遍历格点
     */
    void iterateForQualities();
    void iterateForQualities(long time);

    /**
     * @brief getQuantitiesString 将物理量按照顺序输出为字符串
     */
    void printQuantitiesString(fstream &out);
    void printQuantitiesString();

    /**
     * @brief print 变量值
     */
    void print();

private:
    /**
     * @brief iterateRandom 随机选取格点进行迭代
     */
    void iterateRandom();

    /**
     * @brief iterateTraverse 遍历格点进行迭代
     */
    void iterateTraverse();

    /**
     * @brief 计算（x，y）格点与最近邻格点状态不同的个数
     */
    int countOfDiffStateNearly(int x, int y);

    /**
     * @brief filp 判断格点是否翻转并进行翻转
     */
    bool isFilp(int x, int y);

    /**
     * @brief getSumEnergy 获取当前状态系统总能量
     */
    double getEnergyPerSite();

    /**
     * @brief getEnergy 获取系统能量改变量，用于每步迭代之后
     */
    double getDeltaEnergy(int x, int y);

    /**
     * @brief getSumMagnetisation 获取当前状态系统总磁化强度
     */
    double getMagnetisationPerSite();

    /**
     * @brief getDeltaMagnetisation 获取系统磁化强度改变量，用于每步迭代之后
     */
    double getDeltaMagnetisation(int x, int y);

    /**
     * @brief printSiteState 打印（x，y）格点以及近邻格点的状态
     */
    void printSiteState(int x, int y);

    /**
     * @brief test 测试接口
     */
    void test();

    /**
     * @brief calculateQuantities 计算物理量，计算当前状态物理量并存入Quantities中
     */
    void calculateQuantities();

    /**
     * @brief initQuantities 初始化物理量，预先存入物理量名并置为-1
     */
    void initQuantities();

private:
    vector<vector<int>> state;  //用于存储状态，取值-1或1
    map<string, double> Quantities; //存取物理量
    int row;    //行数
    int col;    //列数
    double temperature; //温度
    long iterationTimes;    //迭代次数
    bool random;    //迭代方法：1随机选择格点、0遍历格点
    double energyState;  //存储上次演化能量
    double magnetisationState;   //存储上次演化磁场强度
};

#endif // ISINGMOUDLESIMULATION_H
