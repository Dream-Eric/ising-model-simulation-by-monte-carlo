//////////////////////////////////////////////////////////////////////
// Copyright(c) 2022-2022 *** All Rights Reserved
// Name:        main.cpp
// Purpose:
// Author:      Shi Yuke
// Modified by:
// Created:     2022-09-25 21:21
// Version:     1.0.0
// Licence:
//////////////////////////////////////////////////////////////////////
#include <random>
#include <time.h>

#include <fstream>

#include "isingmoudlesimulation.h"


int main() {
    //初始化随机数种子
    srand(time(0));

    //打开输出文件
    std::fstream outFile;
    outFile.open("result.txt", std::ios_base::out);

    //预设状态
    IsingMoudleSimulation test(50);
    test.setIterationWays(0);
    test.setIterationTimes(5000);

    //迭代
    for (double temperature = 5.0; temperature > 0.1; temperature -= 0.01) {
//        //预设状态
//        IsingMoudleSimulation test(50);
//        test.setIterationWays(0);
//        test.setIterationTimes(5000);
        test.setTemperature(temperature);

        test.print();
        test.iterateToEquilibrium();
        test.iterateForQualities();

        test.print();
        test.printQuantitiesString(outFile);
    }

    //关闭输出文件
    outFile.close();
    return  0;
}

