#include "isingmoudlesimulation.h"

IsingMoudleSimulation::IsingMoudleSimulation() {
    setSize(10);
    setIterationTimes(100);
    setIterationWays(1);
    setTemperature(2.0);
}

IsingMoudleSimulation::IsingMoudleSimulation(int size) {
    setSize(size);
    setIterationTimes(100);
    setIterationWays(1);
    setTemperature(2.0);
}

//IsingMoudleSimulation::IsingMoudleSimulation(int row, int col) {
//    setSize(row, col);
//    setIterationTimes(100);
//    setIterationWays(1);
//    setTemperature(2.0);
//}

void IsingMoudleSimulation::setSize(int size) {
    state = vector<vector<int>> (size, vector<int>(size) );
    for (auto &rows : state) {
        for (auto &var : rows) {
            //随机对应高温状态，定值对应低温状态
            var = (rand() % 2) ? 1 : -1;
//            var = 1;
        }
    }
    row = col = size;
}

//void IsingMoudleSimulation::setSize(int row, int col) {
//    state = vector<vector<int>> (row, vector<int>(col) );
//    for (auto &rows : state) {
//        for (auto &var : rows) {
//            var = (rand() % 2) ? 1 : -1;
//        }
//    }
//    this->row = row;
//    this->col = col;
//}

void IsingMoudleSimulation::setTemperature(double temperature) {
    this->temperature = temperature;
}

void IsingMoudleSimulation::setIterationTimes(int iterationTimes) {
    this->iterationTimes = iterationTimes;
}

void IsingMoudleSimulation::setIterationWays(bool random) {
    this->random = random;
}

int IsingMoudleSimulation::countOfDiffStateNearly(int x, int y) {
    return int(state[(x - 1 + col) % col][y] != state[x][y]) +
           (state[x][(y - 1 + row) % row] != state[x][y]) +
           (state[(x + 1) % col][y] != state[x][y]) +
           (state[x][(y + 1) % row] != state[x][y]);
}

bool IsingMoudleSimulation::isFilp(int x, int y) {
    int difStateCount = countOfDiffStateNearly(x, y);

    if (4 == difStateCount || 3 == difStateCount || 2 == difStateCount) {
        return true;
    } else if(1 == difStateCount) {
        if (float(rand()) / RAND_MAX < exp(-4.0 / temperature)) {
            return true;
        }
    } else {
        if (float(rand()) / RAND_MAX < exp(-8.0 / temperature)) {
            return true;
        }
    }
    return false;
}

void IsingMoudleSimulation::iterateRandom() {
    int x = rand() % row;
    int y = rand() % col;

    if(isFilp(x, y)) {
        state[x][y] = -state[x][y];
    }
}

void IsingMoudleSimulation::iterateTraverse() {
    for(int x = 0; x < row; ++x) {
        for(int y = 0; y < col; ++y) {
            if(isFilp(x, y)) {
                state[x][y] = -state[x][y];
            }
        }
    }
}

void IsingMoudleSimulation::iterateToEquilibrium() {
    iterateToEquilibrium(iterationTimes);
}

void IsingMoudleSimulation::iterateToEquilibrium(long time) {
    if(random) {
        for(int i = 0; i < time; ++i) {
            iterateRandom();
        }
    } else {
        for(int i = 0; i < time; ++i) {
            iterateTraverse();
        }
    }
    calculateQuantities();
}

void IsingMoudleSimulation::iterateForQualities() {
    iterateForQualities(iterationTimes);
}

void IsingMoudleSimulation::iterateForQualities(long time) {

    double sumOfEnergy = energyState;
    double sumOfMagnetisation = magnetisationState;
    double sumOfSquaresOfEnergy = energyState * energyState;
    double sumOfSquaresOfMagnetisation = magnetisationState * magnetisationState;

    if(random) {
        int x, y;
        for(int i = 0; i < time; ++i) {
            x = rand() % col;
            y = rand() % row;
            if(isFilp(x, y)) {
                energyState += getDeltaEnergy(x, y);
                magnetisationState += getDeltaMagnetisation(x, y);
                state[x][y] = -state[x][y];
            }
            sumOfEnergy += energyState;
            sumOfMagnetisation += magnetisationState;
            sumOfSquaresOfEnergy += energyState * energyState;
            sumOfSquaresOfMagnetisation += magnetisationState * magnetisationState;
        }
    } else {
        for(int i = 0; i < time; ++i) {
            for(int x = 0; x < col; ++x) {
                for(int y = 0; y < col; ++y) {
                    if(isFilp(x, y)) {
                        energyState += getDeltaEnergy(x, y);
                        magnetisationState += getDeltaMagnetisation(x, y);
                        state[x][y] = -state[x][y];
                    }
                    sumOfEnergy += energyState;
                    sumOfMagnetisation += magnetisationState;
                    sumOfSquaresOfEnergy += energyState * energyState;
                    sumOfSquaresOfMagnetisation += magnetisationState * magnetisationState;
                }
            }
        }
        sumOfEnergy /= (col * row);
        sumOfMagnetisation /= (col * row);
        sumOfSquaresOfEnergy /= (col * row);
        sumOfSquaresOfMagnetisation /= (col * row);
    }
    Quantities["EnergyPerSite"] = sumOfEnergy / iterationTimes;
    Quantities["MagnetisationPerSite"] = sumOfMagnetisation / iterationTimes;
    Quantities["SpecificHeat"] =
        sumOfSquaresOfEnergy / iterationTimes - Quantities["EnergyPerSite"] * Quantities["EnergyPerSite"];
    Quantities["MagneticSusceptibility"] =
        sumOfSquaresOfMagnetisation / iterationTimes - Quantities["MagnetisationPerSite"] * Quantities["MagnetisationPerSite"];

}

double IsingMoudleSimulation::getEnergyPerSite() {
    double sumEnergy = 0.0;

    for(int x = 0; x < col; ++x) {
        for(int y = 0; y < col; ++y) {
            sumEnergy -= (state[(x - 1 + col) % col][y] * state[x][y]) +
                         (state[x][(y - 1 + row) % row] * state[x][y]);
        }
    }

    return sumEnergy / (row * col);
}

double IsingMoudleSimulation::getDeltaEnergy(int x, int y) {
    switch (countOfDiffStateNearly(x, y)) {
        case 0:
            return 8.0 / (row * col);
        case 1:
            return 4.0 / (row * col);
        case 2:
            return 0;
        case 3:
            return -4.0 / (row * col);
        case 4:
            return -8.0 / (row * col);
        default:
            return 0;
    }
}

double IsingMoudleSimulation::getMagnetisationPerSite() {
    double sumMagnetisation = 0.0;
    for(int x = 0; x < col; ++x) {
        for(int y = 0; y < col; ++y) {
            sumMagnetisation += state[x][y];
        }
    }
    return sumMagnetisation / (row * col);
}

double IsingMoudleSimulation::getDeltaMagnetisation(int x, int y) {
    return -2.0 * state[x][y] / (row * col);
}

void IsingMoudleSimulation::calculateQuantities() {
    energyState = getEnergyPerSite();
    Quantities["EnergyPerSite"] = energyState;
    magnetisationState = getMagnetisationPerSite();
    Quantities["MagnetisationPerSite"] = magnetisationState;
    Quantities["SpecificHeat"] = -1;
    Quantities["MagneticSusceptibility"] = -1;
}


void IsingMoudleSimulation::initQuantities() {
    Quantities["EnergyPerSite"] = -1;
    Quantities["MagnetisationPerSite"] = -1;
    Quantities["SpecificHeat"] = -1;
    Quantities["MagneticSusceptibility"] = -1;
}

void IsingMoudleSimulation::printQuantitiesString(fstream &out) {
    out << temperature << ' '
        << Quantities["EnergyPerSite"] << ' '
        << Quantities["MagnetisationPerSite"] << ' '
        << Quantities["SpecificHeat"] << ' '
        << Quantities["MagneticSusceptibility"] << endl;
}

void IsingMoudleSimulation::printQuantitiesString() {
    cout << temperature << ' '
         <<  Quantities["EnergyPerSite"] << ' '
         << Quantities["MagnetisationPerSite"] << ' '
         << Quantities["SpecificHeat"] << ' '
         << Quantities["MagneticSusceptibility"] << endl;
}

void IsingMoudleSimulation::printSiteState(int x, int y) {
    cout << "\t" << "state" << "\t" << "row" << "\t" << "col" << endl;
    cout << "cur:\t" << state[x][y] << "\t" << x << "\t" << y << endl;
    cout << "up:\t" << state[(x - 1 + col) % col][y] << "\t" << (x - 1 + col) % col << "\t" << y << endl;
    cout << "left:\t" << state[x][(y - 1 + row) % row] << "\t" << x << "\t" << (y - 1 + row) % row << endl;
    cout << "bottom:\t" << state[(x + 1) % col][y] << "\t" << (x + 1) % col << "\t" << y << endl;
    cout << "right:\t" << state[x][(y + 1) % row] << "\t" << x << "\t" << (y + 1) % row << endl ;
    cout << "countOfDiffStateNearly:" << countOfDiffStateNearly(x, y) << endl << endl;
}

void IsingMoudleSimulation::print() {
    cout << "row:" << row << "  col:" << col
         << "  temperature:" << temperature
         << "  iterationTimes:" << iterationTimes
         << "  filpWay:" << (random == 1 ? "Random" : "Traverse") << endl;
    printQuantitiesString();
    for (auto &rows : state) {
        for (auto &var : rows) {
            cout << ((var == -1 ) ? ' ' : '@') << " ";
        }
        cout << endl;
    }
}

void IsingMoudleSimulation::test() {

}



